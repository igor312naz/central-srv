#!/usr/bin/env bash

php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
php bin/console doctrine:query:sql "SET foreign_key_checks = 0"
php bin/console doctrine:fixtures:load --purge-with-truncate -n
php bin/console doctrine:query:sql "SET foreign_key_checks = 1"
php bin/console api:organization:register -o"Рога и Копыта" -u"http://durakov.net"
